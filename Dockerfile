FROM archlinux:latest

# Update system and install required packages
RUN pacman-db-upgrade
RUN pacman -Syyu --noconfirm
RUN pacman -S --noconfirm perl-template-toolkit chromium make

# Users will mount their working directory to /src
RUN mkdir /src

# Builds will be run as a non-root user 'worker'
RUN useradd worker
RUN chown worker /src

# Switch to the new user and set the working directory
USER worker
WORKDIR /src

# Ensure tpage executable is in the path
ENV PATH=/usr/bin/vendor_perl:$PATH

# Disable chromium sandboxing as it is not supposed in containerized
# environments
ENV PDF_OPTS_EXTRA='--no-sandbox'
